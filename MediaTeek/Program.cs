﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaTeek 
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    class Media
    {
        public string Title { get; set; }
        public string Issuer { get; set; }
        public int IssuedYear { get; set; }
        public List<Item> Items = new List<Item>();
    }
    enum AudioType { CD, Vinyl, Cassette}
    class Audio : Media
    {
        AudioType AudioType;
        public void AddItem(AudioItem ai) => Items.Add(ai);
    }
    enum VideoType { DBD, BR, VHD }
    class Video : Media
    {
        VideoType VideoType;
        public void AddItem(VideoItem vi) => Items.Add(vi);

    }
    class Book : Media
    {
        public void AddItem(BookItem bi) => Items.Add(bi);
    }
    class Item
    {
        public int AuthorId;
        public int PresenterId;
        public Author Author => Author.Get(AuthorId);
        // siia muid andmeid veel
    }
    class AudioItem : Item { }
    class VideoItem : Item { }
    class BookItem : Item { }
    class Author
    {
        static Dictionary<int, Author> _Authors = new Dictionary<int, Author>();
        public IEnumerable<Author> Authors => _Authors.Values;
        static int nr = 0;
        public int AuthorId { get; private set;}
        // siia veel võimalikke andmeid autori kohta
        public Author() => AuthorId = ++nr;

        public static Author Get(int authorId) 
            => _Authors.ContainsKey(authorId) ? _Authors[authorId] : null;
        
    }
    class Presenter { } // umbes samasugune nagu Author
}
